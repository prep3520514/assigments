# Assigments 

This repository is organized as follows: Each folder contains the contents of the individual assignment, named according to the assignment number. Each folder contains the relevant files and might contain a README.md file providing explanations or any additional notes from my end.