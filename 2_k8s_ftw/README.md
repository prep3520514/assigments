# Implementation

1. Build a local kubernetes cluster, for example with [minikube](https://minikube.sigs.k8s.io/docs/start/).

```
minikube start
```

2. Apply the gaia-statefulset.yaml file to the cluster.

```
kubectl apply -f gaia-statefulset.yaml
```