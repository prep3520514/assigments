#!/bin/bash

set -euo pipefail

SCRIPT_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)
INPUT_FILE_PATH=""
# shellcheck disable=SC1091
source "$SCRIPT_DIR/cli/root_command.sh"

function print_pane() {
  local pane_name="$1"
  shift
  local delimiter="===================================="
  echo "$delimiter"
  echo "$pane_name"
  "$@"
  echo "$delimiter"

}
function extract_overview_pane {
  local input_file_path=$1

  total_requests=$(wc -l <"$input_file_path")
  unique_visitors=$(awk '{print $1}' "$input_file_path" | sort -u | wc -l)
  success_requests=$(grep -c '" 200 ' "$input_file_path" )
  failed_requests=$(grep -c -v '" 200 ' "$input_file_path")

  echo "Total Requests: $total_requests"
  echo "Unique Visitors: $unique_visitors"
  echo "Success Requests: $success_requests"
  echo "Failed Requests: $failed_requests"

}

function extract_top_visitors_pane {
  local input_file_path=$1

  top_visitors=$(awk '{print $1}' "$input_file_path" | sort | uniq -c | sort -nr | head -n 10 | awk '{print $1,$2}')
  echo "$top_visitors" | while read -r hits ip; do
    echo "Hits: $hits IP: $ip Geo Location: $(curl -s ipinfo.io/"$ip"/country)"
  done
}

function extract_status_codes_stats_pane(){
  local input_file_path=$1

  sed -E 's/^.*" ([0-9]{3}) .*$/\1/' "$input_file_path"  | sort | uniq -c | sort -nr
}

function main() {
  process_args "$@"

  echo "Analyzing log file: '$INPUT_FILE_PATH'..."
  print_pane "Overview:" extract_overview_pane "$INPUT_FILE_PATH"
  print_pane "Top 10 Visitors IPs:" extract_top_visitors_pane "$INPUT_FILE_PATH"
  print_pane "HTTP Status Codes:" extract_status_codes_stats_pane "$INPUT_FILE_PATH"
}

main "$@"
