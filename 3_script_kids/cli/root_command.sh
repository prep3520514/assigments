#!/bin/bash

SCRIPT_NAME=$(basename "${BASH_SOURCE[0]}")

function print_help {
  echo "$SCRIPT_NAME [options] INPUT_FILE"
  echo "Examples:"
    echo "  $SCRIPT_NAME input.log"
  echo "Outputs an overview of the log file"
  echo
  echo "Options:"
  echo "-h      Print this help message"
  exit
}

function fail {
    printf '%s\n' "$1" >&2 
    exit "${2-1}" 
}

function process_args {
  while getopts ":h:" opt; do
      case $opt in
          h)
              print_help
              ;;
          *)
      esac
  done

  shift $((OPTIND - 1))

  if [ $# -eq 0 ]; then
      fail "Input file is required."
  fi

  INPUT_FILE_PATH=$1

}

export INPUT_FILE_PATH
