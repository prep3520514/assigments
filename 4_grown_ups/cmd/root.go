package cmd

import (
	"analyze/internal/filesearch"
	"analyze/internal/loganalysis"
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "analyze [INPUT_FILE_PATH]",
	Short: "Analyzes log files and provides insights based on the data.",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		inputFilePath := args[0]
		fileSearch, err := filesearch.NewFileSearch(inputFilePath)
		if err != nil {
			panic(err)
		}

		totalRequests, uniqueVisitors, successRequests, failedRequests, err := loganalysis.GetOverviewData(fileSearch)
		if err != nil {
			panic(err)
		}

		// Print overview
		fmt.Println("Overview:")
		fmt.Println("Total Requests:", totalRequests)
		fmt.Println("Unique Visitors:", len(uniqueVisitors))
		fmt.Println("Success Requests:", successRequests)
		fmt.Println("Failed Requests:", failedRequests)

		err = loganalysis.OutputTopVisitors(uniqueVisitors)
		if err != nil {
			panic(err)
		}

		statusCodes, err := loganalysis.StatusCodeStats(fileSearch)
		if err != nil {
			panic(err)
		}
		fmt.Println("Status Codes:")
		for code, count := range statusCodes {
			fmt.Println(code, ":", count)
		}
	},
}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
