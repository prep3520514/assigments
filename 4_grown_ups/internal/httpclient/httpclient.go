package httpclient

import (
	"fmt"
	"io"
	"net/http"
)

func GetCountryByIP(ip string) (string, error) {
	url := fmt.Sprintf("https://ipinfo.io/%s/country", ip)
	resp, err := http.Get(url)
	if err != nil {
		return "", fmt.Errorf("failed to fetch country: %v", err)
	}
	defer resp.Body.Close()

	countryBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("failed to read country response: %v", err)
	}
	return string(countryBytes), nil
}
