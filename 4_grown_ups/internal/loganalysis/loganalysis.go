package loganalysis

import (
	"analyze/internal/filesearch"
	"analyze/internal/httpclient"
	"analyze/internal/utils"
	"fmt"
	"os"
	"sort"
	"strings"
)

func OutputTopVisitors(uniqueVisitors map[string]int) error {
	keys := make([]string, 0, len(uniqueVisitors))
	for k := range uniqueVisitors {
		keys = append(keys, k)
	}

	sort.SliceStable(keys, func(i, j int) bool {
		return uniqueVisitors[keys[i]] > uniqueVisitors[keys[j]]
	})

	fmt.Println("Top Visitors:")
	fmt.Println("Hits | IP Address | Country")
	for i := 0; i < 10 && i < len(keys); i++ {
		ipAddrCountry, err := httpclient.GetCountryByIP(keys[i])
		if err != nil {
			fmt.Println("Failed to get country:", err)
			continue
		}
		fmt.Printf("%d: %s: %s", uniqueVisitors[keys[i]], keys[i], ipAddrCountry)
	}
	return nil
}

func GetOverviewData(fs *filesearch.FileSearch) (totalRequests int, uniqueVisitors map[string]int, successRequests int, failedRequests int, err error) {
	totalRequests, err = GetTotalRequests(fs.FilePath)
	if err != nil {
		return 0, nil, 0, 0, err
	}

	successSearchRes, err := fs.Search("\" 200")
	if err != nil {
		return 0, nil, 0, 0, err
	}
	successRequests = len(successSearchRes.Hits)
	failedRequests = totalRequests - successRequests

	ipAddressesResult, ipAddrErr := fs.Search(`([0-9]{1,3}\.){3}[0-9]{1,3}`)
	if ipAddrErr != nil {
		return 0, nil, 0, 0, ipAddrErr
	}

	uniqueVisitors = make(map[string]int)
	for _, hit := range ipAddressesResult.Hits {
		ipAddr := strings.Fields(hit.Line)[0]
		uniqueVisitors[ipAddr]++
	}
	return totalRequests, uniqueVisitors, successRequests, failedRequests, nil
}

func StatusCodeStats(fs *filesearch.FileSearch) (map[string]int, error) {
	statusCodes := make(map[string]int)
	statusSearchRes, err := fs.Search(`" [0-9]{3}`)
	if err != nil {
		return nil, fmt.Errorf("failed to search status codes: %w", err)
	}
	for _, hit := range statusSearchRes.Hits {
		statusCode := strings.Fields(hit.Line)[8]
		statusCodes[statusCode]++
	}
	return statusCodes, nil
}

func GetTotalRequests(filePath string) (int, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return -1, fmt.Errorf("failed to open file: %w", err)
	}
	defer file.Close()

	count, err := utils.LineCounter(file)
	if err != nil {
		return -1, fmt.Errorf("failed to count lines: %w", err)
	}

	return count, nil
}
