package filesearch

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
)

type FileSearch struct {
	FilePath string
}

type SearchResult struct {
	SearchPattern string
	Hits          []Hit
}

type Hit struct {
	LineNo int
	Line   string
}

func NewFileSearch(filePath string) (*FileSearch, error) {
	fileInfo, err := os.Stat(filePath)
	if err != nil {
		return nil, fmt.Errorf("file not found: %v", err)
	}

	if fileInfo.IsDir() {
		return nil, fmt.Errorf("file search error: %s is a directory", filePath)
	}

	return &FileSearch{filePath}, nil
}

func (fs *FileSearch) Search(pattern string, invertMatch ...bool) (SearchResult, error) {
	var invert bool
	if len(invertMatch) > 0 {
		invert = invertMatch[0]
	}

	f, err := os.Open(fs.FilePath)

	if err != nil {
		return SearchResult{}, fmt.Errorf("open file error: %v", err)
	}

	var hits []Hit

	currentLine := 1
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		matched, _ := regexp.MatchString(pattern, line)
		if invert && !matched || !invert && matched {
			hits = append(hits, Hit{currentLine, line})
		}
		currentLine++
	}

	return SearchResult{pattern, hits}, nil
}

//func (fs *FileSearch) textSearch(pattern string, filePath string, not bool) (SearchResult, error) {
//	f, err := os.Open(filePath)
//
//	if err != nil {
//		return SearchResult{}, fmt.Errorf("open file error: %v", err)
//	}
//
//	var hits []Hit
//
//	currentLine := 1
//	scanner := bufio.NewScanner(f)
//	for scanner.Scan() {
//		line := scanner.Text()
//		matched, _ := regexp.MatchString(pattern, line)
//		if not && !matched || !not && matched {
//			hits = append(hits, Hit{currentLine, line})
//		}
//		currentLine++
//	}
//
//	return SearchResult{filePath, pattern, hits}, nil
//}

//func findFilesAndDirs(baseDir string) ([]string, []string, error) {
//	var dirs []string
//	var files []string
//
//	entries, err := os.ReadDir(baseDir)
//
//	if err != nil {
//		return nil, nil, fmt.Errorf("readdir error: %v", err)
//	}
//
//	for _, entry := range entries {
//		switch entry.IsDir() {
//		case true:
//			dirs = append(dirs, entry.Name())
//		case false:
//			files = append(files, entry.Name())
//		}
//	}
//
//	return dirs, files, nil
//}
