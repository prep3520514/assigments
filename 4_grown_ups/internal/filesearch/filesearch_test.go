package filesearch

import (
	"os"
	"path/filepath"
	"testing"
)

func BenchmarkSearch_SmallFile(b *testing.B) {
	// Arrange
	testDir := createTestFilesWithContent(b, "testdata", []testFile{
		{"file2.txt", "example"},
	})
	defer os.RemoveAll(testDir)
	var testFilePath = filepath.Join(testDir, "file2.txt")
	fs, _ := NewFileSearch(testFilePath)

	// Act and Assert
	for i := 0; i < b.N; i++ {
		_, err := fs.Search("example")
		if err != nil {
			b.Fatalf("Search failed: %v", err)
		}
	}
}

func BenchmarkSearchLargeFile(b *testing.B) {
	// Arrange
	testDir, testFilePath := createLargeTestFile(b, "testdata", "example\n", 1000000)

	defer os.RemoveAll(testDir)
	fs, _ := NewFileSearch(testFilePath)

	// Act and Assert
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, err := fs.Search("example")
		if err != nil {
			b.Fatalf("Search failed: %v", err)
		}
	}
}

func TestSearch_WithExistingPattern_ReturnsResults(t *testing.T) {
	// Arrange
	testDir := createTestFilesWithContent(t, "testdata", []testFile{
		{"file1.txt", "example"},
	})
	defer os.RemoveAll(testDir)

	var testFilePath = filepath.Join(testDir, "file1.txt")
	fs, _ := NewFileSearch(testFilePath)

	// Act
	result, err := fs.Search("example")

	// Assert
	if err != nil {
		t.Fatalf("Search failed: %v", err)
	}

	for _, hit := range result.Hits {
		if !containsString(hit.Line, "example") {
			t.Errorf("Search text not found in hit: %s", hit.Line)
		}
	}
}

func TestSearch_InvertMatch_ReturnsNoResults(t *testing.T) {
	// Arrange
	testDir := createTestFilesWithContent(t, "testdata", []testFile{
		{"file1.txt", "example"},
		{"file2.txt", "example"},
	})
	testFilePath := filepath.Join(testDir, "file1.txt")
	defer os.RemoveAll(testDir)

	fs, _ := NewFileSearch(testFilePath)

	// Act
	result, err := fs.Search("example", true)

	// Assert
	if err != nil {
		t.Fatalf("Search failed: %v", err)
	}

	expectedResults := 0
	if len(result.Hits) != expectedResults {
		t.Errorf("Unexpected number of search results. Got: %d, Expected: %d", len(result.Hits), expectedResults)
	}

}

func TestSearch_WithNonExistingPattern_ReturnsNoResults(t *testing.T) {
	// Arrange
	testDir := createTestFilesWithContent(t, "testdata", []testFile{
		{"file1.txt", "example"},
		{"file2.txt", "example"},
	})
	defer os.RemoveAll(testDir)

	fs, _ := NewFileSearch(testDir)

	// Act
	result, err := fs.Search("non-existing-pattern")

	// Assert
	if err != nil {
		t.Fatalf("Search failed: %v", err)
	}

	if len(result.Hits) != 0 {
		t.Errorf("Unexpected number of search results. Got: %d, Expected: %d", len(result.Hits), 0)
	}
}

func containsString(s, substr string) bool {
	return len(s) >= len(substr) && s[len(s)-len(substr):] == substr
}

func createTestFilesWithContent(tb testing.TB, dir string, files []testFile) string {
	if err := os.MkdirAll(dir, 0755); err != nil {
		tb.Fatalf("Failed to create test directory: %s", err)
	}

	for _, file := range files {
		filePath := filepath.Join(dir, file.Name)
		f, err := os.Create(filePath)
		if err != nil {
			tb.Fatalf("Failed to create test file: %s", err)
		}

		// Write content to the file
		if _, err := f.WriteString(file.Content + "\n"); err != nil {
			tb.Fatalf("Failed to write content to test file: %s", err)
		}
		defer f.Close()
	}

	return dir
}

func createLargeTestFile(tb testing.TB, dir, content string, lines int) (string, string) {
	testDir := filepath.Join(dir, "testdata")
	if err := os.MkdirAll(testDir, 0755); err != nil {
		tb.Fatalf("Failed to create test directory: %s", err)
	}

	testFilePath := filepath.Join(testDir, "large_test_file.txt")
	f, err := os.Create(testFilePath)
	if err != nil {
		tb.Fatalf("Failed to create large test file: %s", err)
	}
	defer f.Close()

	for i := 0; i < lines; i++ {
		if _, err := f.WriteString(content); err != nil {
			tb.Fatalf("Failed to write content to large test file: %s", err)
		}
	}

	return testDir, testFilePath
}

type testFile struct {
	Name    string
	Content string
}
